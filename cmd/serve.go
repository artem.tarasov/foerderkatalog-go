package cmd

import (
	"context"
	"database/sql"
	"log"
	"net/http"
	"os"

	"devopps.de/fk/fk"
	"github.com/labstack/echo/v4"
	"github.com/spf13/cobra"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/sqlitedialect"
	"github.com/uptrace/bun/driver/sqliteshim"
	"github.com/uptrace/bun/extra/bundebug"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/zipkin"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"

	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	"golang.org/x/exp/slices"
)

var DbPath string
var Debug bool
var BasePath string
var ZipkinURL string

func init() {
	serveCmd.Flags().StringVarP(&DbPath, "db", "i", "fk.sqlite", "Sqlite DB filename")
	serveCmd.Flags().BoolVar(&Debug, "debug", false, "Run in debug mode")
	serveCmd.Flags().StringVarP(&BasePath, "base-path", "p", "/", "URL base path")
	serveCmd.Flags().StringVar(&ZipkinURL, "zipkin", "http://localhost:9411/api/v2/spans", "Zipkin URL")
	rootCmd.AddCommand(serveCmd)
}

var tracer trace.Tracer

type FkContext struct {
	echo.Context

	controller *fk.Controller
}

func controller(c echo.Context) *fk.Controller {
	return c.(*FkContext).controller
}

func ctx(c echo.Context) context.Context {
	return c.Request().Context()
}

func json(c echo.Context, i interface{}) error {
	return c.JSONPretty(http.StatusOK, i, "  ")
}

func getId(c echo.Context) (int64, error) {
	var id int64
	if err := echo.PathParamsBinder(c).Int64("id", &id).BindError(); err != nil {
		return 0, err
	} else {
		return id, nil
	}
}

func orgFindHandler(c echo.Context) error {
	name := c.QueryParam("name")

	if orgs, err := controller(c).Org.FindByName(ctx(c), name); err != nil {
		return err
	} else {
		return json(c, orgs)
	}
}

func orgHandler(c echo.Context) error {
	if id, err := getId(c); err != nil {
		return err
	} else if org, err := controller(c).Org.FindById(ctx(c), id); err != nil {
		return err
	} else {
		return json(c, org)
	}
}

func orgProjectsHandler(c echo.Context) error {
	if id, err := getId(c); err != nil {
		return err
	} else if projects, err := controller(c).Org.GetProjects(ctx(c), id); err != nil {
		return err
	} else {
		return json(c, projects)
	}
}

func orgCollaborationsHandler(c echo.Context) error {
	if id, err := getId(c); err != nil {
		return err
	} else if projects, err := controller(c).Org.GetCollaborations(ctx(c), id); err != nil {
		return err
	} else {
		return json(c, projects)
	}
}

func jpHandler(c echo.Context) error {
	if id, err := getId(c); err != nil {
		return err
	} else if jp, err := controller(c).JointProject.FindById(ctx(c), id); err != nil {
		return err
	} else {
		return json(c, jp)
	}
}

func jpProjectsHandler(c echo.Context) error {
	if id, err := getId(c); err != nil {
		return err
	} else if projects, err := controller(c).JointProject.GetProjects(ctx(c), id); err != nil {
		return err
	} else {
		return json(c, projects)
	}
}

func locOrganizationsHandler(c echo.Context) error {
	id := fk.Location(c.Param("id"))
	if orgs, err := controller(c).Location.GetOrganizations(ctx(c), id); err != nil {
		return err
	} else {
		return json(c, orgs)
	}
}

func locProjectsHandler(c echo.Context) error {
	id := fk.Location(c.Param("id"))
	if projects, err := controller(c).Location.GetProjects(ctx(c), id); err != nil {
		return err
	} else {
		return json(c, projects)
	}
}

func multiEdgesHandler(c echo.Context) error {
	nc := controller(c).Network

	type Response struct {
		Nodes fk.OrgList
		Edges []fk.NodeSet
	}

	var params fk.GetMultiEdgesParams
	var response Response
	var err error

	if err = c.Bind(&params); err != nil {
		return err
	}

	parentCtx, span := tracer.Start(ctx(c), c.Path(), trace.WithAttributes(
		attribute.String("locationId", params.LocationId),
		attribute.Int64Slice("recipientIds", params.RecipientIds),
		attribute.Int64Slice("researchAreaIds", params.ResearchAreaIds),
		attribute.Int("maxEdges", params.MaxEdges),
		attribute.Int("maxNodes", params.MaxNodes),
	))
	defer span.End()

	_, dbEdgeSpan := tracer.Start(parentCtx, "multi_edges_select_edges")
	response.Edges, err = nc.GetMultiEdges(ctx(c), params)
	dbEdgeSpan.End()

	if err != nil {
		return err
	}

	var ids []int64
	for _, e := range response.Edges {
		ids = append(ids, e.NodeIds...)
	}

	// lol generics
	slices.Sort(ids)
	ids = slices.Compact(ids)

	_, dbNodeSpan := tracer.Start(parentCtx, "multi_edges_select_nodes")
	defer dbNodeSpan.End()

	if nodes, err := controller(c).Org.FindByIds(ctx(c), ids); err != nil {
		return err
	} else {
		response.Nodes = nodes
	}
	return json(c, response)
}

func addHandlers(e *echo.Echo) {
	g := e.Group(BasePath)
	g.GET("/organizations/find", orgFindHandler)
	g.GET("/organizations/:id", orgHandler)
	g.GET("/organizations/:id/projects", orgProjectsHandler)
	g.GET("/organizations/:id/collaborations", orgCollaborationsHandler)

	g.GET("/joint-projects/:id", jpHandler)
	g.GET("/joint-projects/:id/projects", jpProjectsHandler)

	g.GET("/locations/:id/organizations", locOrganizationsHandler)
	g.GET("/locations/:id/projects", locProjectsHandler)

	g.GET("/recipient-network/multi-edges", multiEdgesHandler)
}

// based on https://github.com/open-telemetry/opentelemetry-go/blob/main/example/zipkin/main.go and
// https://github.com/open-telemetry/opentelemetry-go-contrib/blob/main/instrumentation/github.com/labstack/echo/otelecho/example/server.go
func initTracer(url string) (func(context.Context) error, error) {
	exporter, err := zipkin.New(url)
	if err != nil {
		return nil, err
	}

	tp := sdktrace.NewTracerProvider(
		sdktrace.WithBatcher(exporter),
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithResource(resource.NewSchemaless(
			attribute.String("service.name", "fk-serve"),
		)),
	)

	p := propagation.NewCompositeTextMapPropagator(
		propagation.TraceContext{},
		propagation.Baggage{},
	)

	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(p)

	return tp.Shutdown, nil
}

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "start a server on http://localhost:3000",

	Run: func(cmd *cobra.Command, args []string) {
		logger := log.New(os.Stderr, "fk-serve", log.Ldate|log.Ltime|log.Llongfile)

		sqldb, err := sql.Open(sqliteshim.ShimName, DbPath)
		if err != nil {
			logger.Fatal(err)
		}

		tracerShutdown, err := initTracer(ZipkinURL)
		if err != nil {
			logger.Fatal(err)
		}
		defer func() {
			if err := tracerShutdown(context.Background()); err != nil {
				logger.Fatal(err)
			}
		}()

		db := bun.NewDB(sqldb, sqlitedialect.New())
		db.AddQueryHook(bundebug.NewQueryHook(bundebug.WithVerbose(Debug)))

		tracer = otel.Tracer("main")
		controller := fk.NewController(db)

		e := echo.New()
		e.Debug = Debug
		e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
			return func(c echo.Context) error {
				cc := &FkContext{c, controller}
				return next(cc)
			}
		})

		addHandlers(e)

		e.Logger.Fatal(e.Start(":3000"))
	},
}
