package cmd

import (
	"context"
	"database/sql"
	"os"
	"reflect"
	"runtime"
	"runtime/pprof"
	"strings"

	"devopps.de/fk/fk"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"

	"github.com/uptrace/bun"

	"github.com/uptrace/bun/dialect/sqlitedialect"
	"github.com/uptrace/bun/driver/sqliteshim"
	"github.com/uptrace/bun/extra/bundebug"
)

func initializeTable[T any](ctx context.Context, db *bun.DB) error {
	_, err := db.NewCreateTable().Model((*T)(nil)).Exec(ctx)
	return err
}

func indexQuery[T any](db *bun.DB, column string) *bun.CreateIndexQuery {
	var model T
	indexName := strings.ToLower(reflect.TypeOf(model).Name()) + "_" + column + "_idx"
	return db.NewCreateIndex().Model(&model).Index(indexName).Column(column)
}

func createTables(ctx context.Context, db *bun.DB) error {
	if err := initializeTable[fk.ResearchArea](ctx, db); err != nil {
		return err
	} else if err := initializeTable[fk.ResearchTopic](ctx, db); err != nil {
		return err
	} else if _, err := indexQuery[fk.ResearchTopic](db, "code").Unique().Exec(ctx); err != nil {
		return err
	} else if err := initializeTable[fk.Organization](ctx, db); err != nil {
		return err
	} else if _, err := indexQuery[fk.Organization](db, "name").Unique().Exec(ctx); err != nil {
		return err
	} else if _, err := indexQuery[fk.Organization](db, "municipality_id").Exec(ctx); err != nil {
		return err
	} else if err := initializeTable[fk.JointProject](ctx, db); err != nil {
		return err
	} else if err := initializeTable[fk.Project](ctx, db); err != nil {
		return err
	}

	return nil
}

func populateResearchAreaTable(ctx context.Context, db *bun.DB) error {
	research_areas := fk.RESEARCH_AREAS[:]
	_, err := db.NewInsert().Model(&research_areas).Exec(ctx)
	return err
}

func saveAll(csv fk.CsvReader, projects []*fk.Project, db *bun.DB) error {
	orgs := csv.GetOrganizations()
	researchTopics := csv.GetResearchTopics()
	jps := csv.GetJointProjects(runtime.NumCPU())

	ctx := context.Background()
	if _, err := db.NewInsert().Model(&orgs).Exec(ctx); err != nil {
		log.Error().Err(err).Msg("failed to populate organizations table")
		return err
	}

	if _, err := db.NewInsert().Model(&researchTopics).Exec(ctx); err != nil {
		log.Error().Err(err).Msg("failed to populate research topics table")
	}

	if _, err := db.NewInsert().Model(&jps).Exec(ctx); err != nil {
		log.Error().Err(err).Msg("failed to populate joint projects table")
		return err
	}

	if _, err := db.NewInsert().Model(&projects).Exec(ctx); err != nil {
		log.Error().Err(err).Msg("failed to populate projects table")
		return err
	}

	return nil
}

var CsvPath string
var OutputPath string
var CpuProfilePath string

func importCsv() {
	sqldb, err := sql.Open(sqliteshim.ShimName, OutputPath)
	if err != nil {
		panic(err)
	}

	db := bun.NewDB(sqldb, sqlitedialect.New())

	bundebug.NewQueryHook(
		bundebug.WithEnabled(false),
		// BUNDEBUG=1 logs failed queries
		// BUNDEBUG=2 logs all queries
		bundebug.FromEnv("BUNDEBUG"),
	)

	if CpuProfilePath != "" {
		f, err := os.Create(CpuProfilePath)
		if err != nil {
			log.Error().Err(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	ctx := context.Background()

	if err := createTables(ctx, db); err != nil {
		panic(err)
	}

	if err := populateResearchAreaTable(ctx, db); err != nil {
		panic(err)
	}

	if csv, err := fk.OpenCsv(CsvPath); err != nil {
		panic(err)
	} else if projects, err := csv.ReadProjects(); err != nil {
		panic(err)
	} else if err := saveAll(csv, projects, db); err != nil {
		panic(err)
	}
}

func init() {
	importCmd.Flags().StringVarP(&CsvPath, "in", "i", "Suchliste.csv", "Input CSV filename")
	importCmd.Flags().StringVarP(&OutputPath, "out", "o", "fk.sqlite", "Output filename")
	importCmd.Flags().StringVar(&CpuProfilePath, "cpuprofile", "", "CPU profile filename")
	rootCmd.AddCommand(importCmd)
}

var importCmd = &cobra.Command{
	Use:   "import",
	Short: "import CSV downloaded from https://foerderportal.bund.de/foekat/",
	Run: func(cmd *cobra.Command, args []string) {
		importCsv()
	},
}
