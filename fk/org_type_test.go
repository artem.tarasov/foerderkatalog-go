package fk

import "testing"

func TestGuessOrgType(t *testing.T) {
	if ot := GuessOrgType("gemeinnützige KIMW Forschungs-GmbH"); ot != GGMBH {
		t.Fatal(ot)
	}

	if ot := GuessOrgType("Verbandsgemeinde Mansfelder Grund - Helbra"); ot != MUNICIPALITY {
		t.Fatal(ot)
	}

	if ot := GuessOrgType("Gemeinde Rainau"); ot != MUNICIPALITY {
		t.Fatal(ot)
	}

	if ot := GuessOrgType("Bayerische Motoren Werke Aktiengesellschaft"); ot != AG {
		t.Fatal(ot)
	}

	if ot := GuessOrgType("Fraunhofer-Gesellschaft zur Förderung der angewandten Forschung eingetragener Verein"); ot != CLUB {
		t.Fatal(ot)
	}

	if ot := GuessOrgType("YNCORIS GmbH & Co. KG"); ot != KG {
		t.Fatal(ot)
	}

	if ot := GuessOrgType("Stadt Wilhelmshaven"); ot != CITY {
		t.Fatal(ot)
	}

	if ot := GuessOrgType("acatech - Deutsche Akademie der Technikwissenschaften e.V."); ot != CLUB {
		t.Fatal(ot)
	}

	if ot := GuessOrgType("Berliner Stadtreinigungsbetriebe (BSR)"); ot != OTHER {
		t.Fatal(ot)
	}

	if ot := GuessOrgType("VELOMAT Messelektronik GmbH"); ot != GMBH {
		t.Fatal(ot)
	}

	if ot := GuessOrgType("Ruhr-Universität Bochum"); ot != UNIVERSITY {
		t.Fatal(ot)
	}

	if ot := GuessOrgType("Rheinisch-Westfälische Technische Hochschule Aachen"); ot != UNIVERSITY {
		t.Fatal(ot)
	}
}
