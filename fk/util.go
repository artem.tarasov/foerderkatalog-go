package fk

import (
	"regexp"
	"strings"
	"unicode"

	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

var spaceRegexp = regexp.MustCompile(`\s+`)
var vpRegexp = regexp.MustCompile(`[-:\s]*(VP|Verbund(vorhaben|projekt)?)[-:\s]*`)
var tpRegexp = regexp.MustCompile(`[-,\s]+(TV|Teilvorhaben|TP|Teilprojekt|Vorhaben)[-:\d\s.]*`)

func preprocessTopic(topic string) string {
	s := strings.ReplaceAll(topic, " :", ":")
	s = strings.ReplaceAll(s, ": -", ": ")
	s = spaceRegexp.ReplaceAllLiteralString(s, " ")
	s = vpRegexp.ReplaceAllLiteralString(s, " ")
	s = tpRegexp.ReplaceAllLiteralString(s, " Teilprojekt ")
	return strings.TrimLeft(s, " ")
}

var r1 = regexp.MustCompile(`[-,\s]*teilprojekt.*$`)
var r2 = regexp.MustCompile(`[-_,;]\s*$`)

type TrieUtil struct {
	accentRemover transform.Transformer
}

func NewTrieUtil() *TrieUtil {
	return &TrieUtil{
		transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC),
	}
}

func (tu *TrieUtil) stripAccents(s string) string {
	result, _, _ := transform.String(tu.accentRemover, s)
	return result
}

func (tu *TrieUtil) NormalizeTopic(topic string) string {
	return strings.ToLower(tu.stripAccents(preprocessTopic(topic)))
}

// best attempt to extract joint project description out of individual project topics
func (tu *TrieUtil) CommonDescription(topics []string) string {
	trie := NewTrie()

	var normalizedTopics []string
	for _, topic := range topics {
		normalized := tu.NormalizeTopic(topic)
		normalizedTopics = append(normalizedTopics, normalized)
		trie.Add(normalized)
	}

	commonPrefix := trie.CommonPrefix()
	commonPrefix = r1.ReplaceAllLiteralString(commonPrefix, "")
	commonPrefix = r2.ReplaceAllLiteralString(commonPrefix, "")

	description := ""
	for i, topic := range normalizedTopics {
		if strings.HasPrefix(topic, commonPrefix) {
			commonPrefixLength := len([]rune(commonPrefix))
			runes := []rune(preprocessTopic(topics[i]))
			description = string(runes[:commonPrefixLength])
			break
		}
	}

	return description
}
