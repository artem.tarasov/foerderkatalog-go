package fk

import "strings"

type OrganizationType string

// potential future additions: IHK, Landkreis, Institut, Museum, SE, Amt
const (
	GMBH         = OrganizationType("GmbH")
	GGMBH        = OrganizationType("gGmbH")
	AG           = OrganizationType("AG")
	KG           = OrganizationType("KG")
	KGAA         = OrganizationType("KGaA")
	OHG          = OrganizationType("OHG")
	UG           = OrganizationType("UG")
	CLUB         = OrganizationType("Verein")
	EG           = OrganizationType("eG")
	EWIV         = OrganizationType("EWIV")
	UNIVERSITY   = OrganizationType("Hochschule")
	CITY         = OrganizationType("Stadt")
	MUNICIPALITY = OrganizationType("Gemeinde")
	OTHER        = OrganizationType("sonst.")
)

func (t OrganizationType) IsPublic() bool {
	return t == UNIVERSITY || t == CITY || t == MUNICIPALITY || t == OTHER
}

func isLimitedLiability(name string) bool {
	return strings.Contains(name, " mbH") ||
		strings.Contains(name, "mit beschränkter Haftung") ||
		strings.Contains(name, " GmbH") ||
		strings.Contains(name, "-GmbH") ||
		strings.Contains(name, " gmbh") ||
		strings.Contains(name, "-gmbh")
}

func GuessOrgType(name string) OrganizationType {
	if strings.Contains(name, " KGaA") {
		return KGAA
	} else if strings.Contains(name, " KG") {
		return KG
	} else if strings.Contains(name, " OHG") || strings.Contains(name, " oHG") {
		return OHG
	} else if strings.Contains(name, " eG") {
		return EG
	} else if strings.Contains(name, " gGmbH") ||
		(strings.Contains(name, "gemeinnützige") && isLimitedLiability(name)) {
		return GGMBH
	} else if isLimitedLiability(name) {
		return GMBH
	} else if strings.Contains(name, " AG") || strings.Contains(name, "Aktiengesellschaft") {
		return AG
	} else if strings.Contains(name, " UG") {
		return UG
	} else if strings.Contains(name, " EWIV") {
		return EWIV
	}

	lname := strings.ToLower(name)

	if strings.Contains(lname, "e.v.") ||
		strings.Contains(lname, "e. v.") ||
		strings.Contains(lname, "verein") {
		return CLUB
	} else if strings.Contains(lname, "universität") ||
		strings.Contains(lname, "hochschule") ||
		strings.Contains(lname, "university") {
		return UNIVERSITY
	} else if strings.Contains(lname, "stadt ") {
		return CITY
	} else if strings.Contains(lname, "gemeinde") {
		return MUNICIPALITY
	} else {
		return OTHER
	}
}
