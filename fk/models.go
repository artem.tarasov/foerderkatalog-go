package fk

import (
	"time"

	"github.com/shopspring/decimal"
	"github.com/uptrace/bun"
)

type ResearchArea struct {
	bun.BaseModel `bun:"table:research_areas,alias:ra"`

	ID          int64 `bun:"id,pk"`
	Code        string
	Description string
}

type Organization struct {
	bun.BaseModel `bun:"table:organizations,alias:o"`

	ID   int64  `bun:"id,pk"`
	Name string `bun:",unique"`
	Type string

	MunicipalityId string
	Municipality   string
	Location       string
	State          string
	Country        string
}

type JointProject struct {
	bun.BaseModel `bun:"table:joint_projects,alias:jp"`

	ID          int64 `bun:"id,pk"`
	Name        string
	Description string

	// all of these could be done in SQL but I'd rather precompute them
	StartDate     time.Time
	EndDate       time.Time
	ProjectCount  int64
	FundingAmount decimal.Decimal
}

type Project struct {
	bun.BaseModel `bun:"table:projects,alias:p"`

	ID              int64 `bun:"id,pk,autoincrement"`
	Fkz             string
	Ministry        string
	Department      string
	ProjectSponsor  string
	WorkUnit        string
	RecipientId     int64
	ExecutingBodyId int64
	Topic           string
	ResearchTopicId int64
	StartDate       time.Time
	EndDate         time.Time
	FundingAmount   decimal.Decimal
	FundingProfile  string
	FundingType     string
	JointProjectId  *int64

	ResearchTopic *ResearchTopic `bun:"rel:belongs-to,join:research_topic_id=id"`
	Recipient     *Organization  `bun:"rel:belongs-to,join:recipient_id=id"`
	ExecutingBody *Organization  `bun:"rel:belongs-to,join:executing_body_id=id"`
	JointProject  *JointProject  `bun:"rel:belongs-to,join:joint_project_id=id"`
}

type ResearchTopic struct {
	bun.BaseModel `bun:"table:research_topics,alias:rt"`

	ID          int64  `bun:"id,pk,autoincrement"`
	Code        string `bun:",unique"`
	Description string

	ResearchAreaId int64
	ResearchArea   *ResearchArea `bun:"rel:belongs-to,join:research_area_id=id"`
}

type Location string

type NodeSet struct {
	NodeIds      []int64
	Multiplicity int
	TotalFunding []decimal.Decimal
}
