package fk

import "strings"

// the prefix must be common to more than half of all inputs
const THRESHOLD = 0.5

type TrieNode struct {
	children        map[rune]*TrieNode
	numberOfMatches int // how many entries share this prefix
}

type Trie struct {
	root *TrieNode
}

func NewTrie() *Trie {
	trie := new(Trie)
	trie.root = NewTrieNode()
	return trie
}

func NewTrieNode() *TrieNode {
	node := new(TrieNode)
	node.children = make(map[rune]*TrieNode)
	node.numberOfMatches = 0
	return node
}

func (t Trie) Add(str string) {
	var node = t.root
	for _, ch := range str {
		child, ok := node.children[ch]
		if !ok {
			child = NewTrieNode()
			node.children[ch] = child
		}

		// update at each level
		node.numberOfMatches += 1
		node = child
	}

	node.numberOfMatches += 1
}

func (t Trie) CommonPrefix() string {
	var node *TrieNode
	var next = t.root

	var str strings.Builder

	target := float64(t.root.numberOfMatches) * THRESHOLD

	for {
		node = next

		for ch, child := range node.children {
			if float64(child.numberOfMatches) > target {
				str.WriteRune(ch)
				next = child
				break
			}
		}

		if node == next {
			break
		}
	}

	return str.String()
}
