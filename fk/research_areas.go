package fk

// sources:
// https://www.datenportal.bmbf.de/portal/de/Tabelle-1.1.6.html
// https://www.kerndatensatz-forschung.de/docs_ff/listen_leistungsplansystematik_des_bundes.xlsx
var RESEARCH_AREAS = [...]ResearchArea{
	{ID: 0, Code: "AA", Description: "Gesundheitsforschung und Gesundheitswirtschaft"},
	{ID: 1, Code: "AB", Description: "Strahlenschutz"},
	{ID: 2, Code: "B", Description: "Bioökonomie"},
	{ID: 3, Code: "C", Description: "Zivile Sicherheitsforschung"},
	{ID: 4, Code: "DA", Description: "Ernährung"},
	{ID: 5, Code: "DB", Description: "Nachhaltige Agrarwirtschaft und ländliche Räume"},
	{ID: 6, Code: "DC", Description: "Gesundheitlicher und wirtschaftlicher Verbraucherschutz"},
	{ID: 7, Code: "EA", Description: "Rationelle Energieumwandlung"},
	{ID: 8, Code: "EB", Description: "Erneuerbare Energien"},
	{ID: 9, Code: "EC", Description: "Kerntechnische Sicherheit und Entsorgung"},
	{ID: 10, Code: "ED", Description: "Beseitigung kerntechnischer Anlagen"},
	{ID: 11, Code: "EF", Description: "Fusionsforschung"},
	{ID: 12, Code: "FA", Description: "Klima, Klimaschutz; Globaler Wandel"},
	{ID: 13, Code: "FB", Description: "Küsten-, Meeres- und Polarforschung, Geowissenschaften"},
	{ID: 14, Code: "FC", Description: "Umwelt- und Nachhaltigkeitsforschung"},
	{ID: 15, Code: "FD", Description: "Ökologie, Naturschutz, nachhaltige Nutzung"},
	{ID: 16, Code: "GA", Description: "Softwaresysteme; Wissenstechnologien"},
	{ID: 17, Code: "GB", Description: "Kommunikationstechnologien und -dienste"},
	{ID: 18, Code: "GC", Description: "Elektronik und Elektroniksysteme"},
	{ID: 19, Code: "GD", Description: "Mikrosystemtechnik"},
	{ID: 20, Code: "GE", Description: "Multimedia - Entwicklung konvergenter IKT"},
	{ID: 21, Code: "HA", Description: "Fahrzeug- und Verkehrstechnologien"},
	{ID: 22, Code: "HB", Description: "Maritime Technologien"},
	{ID: 50, Code: "IA", Description: "Luftfahrt"},
	{ID: 23, Code: "IB", Description: "Nationale Weltraumforschung und Weltraumtechnik"},
	{ID: 24, Code: "JA", Description: "Forschung zur Verbesserung der Arbeitsbedingungen"},
	{ID: 25, Code: "JB", Description: "Forschung im Dienstleistungssektor"},
	{ID: 26, Code: "KA", Description: "Nanotechnologien"},
	{ID: 27, Code: "KB", Description: "Werkstofftechnologien"},
	{ID: 28, Code: "L", Description: "Optische Technologien"},
	{ID: 29, Code: "M", Description: "Produktionstechnologien"},
	{ID: 30, Code: "NA", Description: "Raumordnung, Stadtentwicklung und Wohnen"},
	{ID: 31, Code: "NB", Description: "Bauforschung"},
	{ID: 32, Code: "OA", Description: "Bildungsberichtserstattung, internationale Assessments"},
	{ID: 33, Code: "OB", Description: "Forschung in der Bildung"},
	{ID: 34, Code: "OC", Description: "Neue Medien in der Bildung"},
	{ID: 35, Code: "PA", Description: "Geisteswissenschaftliche Forschung"},
	{ID: 36, Code: "PB", Description: "Sozialwissenschaftliche Forschung"},
	{ID: 37, Code: "PC", Description: "Wirtschafts- und finanzwissenschaftliche Forschung"},
	{ID: 38, Code: "PD", Description: "Infrastrukturen"},
	{ID: 53, Code: "PG", Description: "Forschung zu Familien, Kinder, Jugend"}, // from data
	{ID: 54, Code: "PI", Description: "Forschung zu Integration, Migration"},   // from data
	{ID: 55, Code: "PJ", Description: "Forschung zu Gemeinwohl, Entwicklung"},  // from data
	{ID: 56, Code: "QA", Description: "Stärkung der Gründungskultur"},          // from data
	{ID: 39, Code: "QB", Description: "Technologieförderung des Mittelstandes"},
	{ID: 40, Code: "QC", Description: "Technologietransfer und Innovationsberatung"},
	{ID: 41, Code: "RA", Description: "Technikfolgenabschätzung"},
	{ID: 42, Code: "RB", Description: "Strukturelle Querschnittsaktivitäten"},
	{ID: 43, Code: "RC", Description: "Demographischer Wandel"},
	{ID: 44, Code: "RD", Description: "Sportförderung und Sportforschung"},
	{ID: 45, Code: "RE", Description: "Sonstiges"},
	{ID: 46, Code: "TA", Description: "Grundfinanzierung von Forschungseinrichtungen"},
	{ID: 47, Code: "TB", Description: "Sonstiges"},
	{ID: 48, Code: "U", Description: "Großgeräte der Grundlagenforschung"},
	{ID: 51, Code: "V", Description: "Wirtschaftsförderung (nicht FuE-relevant)"},
	{ID: 52, Code: "Y", Description: "Nicht FuE-relevante Bildungsausgaben - keine Wissenschaftsausgaben"},
	{ID: 49, Code: "Z", Description: "Globale Minderausgabe; Planungsreserve"},
}

var researchAreaCodeToId = func() map[string]int64 {
	mapping := make(map[string]int64)
	for _, ra := range RESEARCH_AREAS {
		mapping[ra.Code] = ra.ID
	}
	return mapping
}()

func getResearchAreaId(focusAreaId string) *int64 {
	if len(focusAreaId) < 2 {
		return nil
	}

	if id, ok := researchAreaCodeToId[focusAreaId[0:2]]; ok {
		return &id
	} else if id, ok := researchAreaCodeToId[focusAreaId[0:1]]; ok {
		return &id
	}

	return nil
}
