package fk

import (
	"context"

	"github.com/shopspring/decimal"
	"github.com/uptrace/bun"
)

type OrgController struct {
	db *bun.DB
}

type Context = context.Context
type OrgList = []*Organization
type PrjList = []*Project

func (oc *OrgController) FindByName(ctx Context, name string) (OrgList, error) {
	orgs := make(OrgList, 0)

	err := oc.db.NewSelect().
		Model(&orgs).
		Where("? LIKE ?", bun.Ident("name"), "%"+name+"%").
		Scan(ctx)

	return orgs, err
}

func (oc *OrgController) FindById(ctx Context, id int64) (*Organization, error) {
	var org Organization
	err := oc.db.NewSelect().Model(&org).Where("id = ?", id).Scan(ctx)
	return &org, err
}

func (oc *OrgController) FindByIds(ctx Context, ids []int64) (OrgList, error) {
	orgs := make(OrgList, 0)

	err := oc.db.NewSelect().
		Model(&orgs).
		Where("id in (?)", bun.In(ids)).
		Scan(ctx)

	return orgs, err
}

func (oc *OrgController) GetProjects(ctx Context, id int64) (PrjList, error) {
	projects := make(PrjList, 0)
	err := oc.db.NewSelect().Model(&projects).
		Where("recipient_id = ?", id).
		WhereOr("executing_body_id = ?", id).
		Relation("Recipient").
		Relation("ExecutingBody").
		Relation("JointProject").
		Relation("ResearchTopic").
		Relation("ResearchTopic.ResearchArea").
		Scan(ctx)

	return projects, err
}

// projects that share a joint project where organization is the executing body;
// the result doesn't include joint project data, use GetProjects for that.
func (oc *OrgController) GetCollaborations(ctx Context, id int64) (PrjList, error) {
	var projects PrjList

	subq := oc.db.NewSelect().Model(&projects).
		Where("executing_body_id = ?", id).
		Column("joint_project_id").
		Distinct()

	err := oc.db.NewSelect().Model(&projects).
		Where("joint_project_id in (?)", subq).
		Where("executing_body_id <> ?", id).
		Relation("Recipient").
		Relation("ExecutingBody").
		Relation("ResearchTopic").
		Relation("ResearchTopic.ResearchArea").
		Scan(ctx)

	return projects, err
}

type JointProjectController struct {
	db *bun.DB
}

func (jpc *JointProjectController) FindById(ctx Context, id int64) (*JointProject, error) {
	var jp JointProject
	err := jpc.db.NewSelect().Model(&jp).Where("id = ?", id).Scan(ctx)
	return &jp, err
}

func (jpc *JointProjectController) GetProjects(ctx Context, id int64) (PrjList, error) {
	projects := make(PrjList, 0)
	err := jpc.db.NewSelect().Model(&projects).
		Where("joint_project_id = ?", id).
		Relation("Recipient").
		Relation("ExecutingBody").
		Relation("ResearchTopic").
		Relation("ResearchTopic.ResearchArea").
		Scan(ctx)

	return projects, err
}

type LocationController struct {
	db *bun.DB
}

func (lc *LocationController) GetOrganizations(ctx Context, loc Location) (OrgList, error) {
	orgs := make(OrgList, 0)

	err := lc.db.NewSelect().
		Model(&orgs).
		Where("municipality_id like ?", string(loc)+"%").
		Scan(ctx)

	return orgs, err
}

func (lc *LocationController) GetProjects(ctx Context, loc Location) (PrjList, error) {
	projects := make(PrjList, 0)

	err := lc.db.NewSelect().
		Model(&projects).
		Relation("Recipient").
		Relation("ExecutingBody").
		Relation("ResearchTopic").
		Relation("ResearchTopic.ResearchArea").
		Relation("JointProject").
		Where("executing_body_id.municipality_id LIKE ?", string(loc)+"%").
		Scan(ctx)

	return projects, err
}

type NetworkController struct {
	db *bun.DB
}

type GetMultiEdgesParams struct {
	MaxEdges        int     `query:"max-edges"`
	MaxNodes        int     `query:"max-nodes"`
	LocationId      string  `query:"lid"`
	ResearchAreaIds []int64 `query:"ra"`
	RecipientIds    []int64 `query:"rid"`
}

func (nc *NetworkController) GetMultiEdges(ctx Context, params GetMultiEdgesParams) ([]NodeSet, error) {
	type multiEdge struct {
		O1           int64
		O2           int64
		Multiplicity int64
		F1           decimal.Decimal
		F2           decimal.Decimal
	}

	multiEdges := make([]multiEdge, 0)
	nodeSets := make([]NodeSet, 0)

	var o1, o2 bun.Ident
	if len(params.RecipientIds) == 0 {
		o1 = bun.Ident("p1.recipient_id")
		o2 = bun.Ident("p2.recipient_id")
	} else {
		o1 = bun.Ident("p1.executing_body_id")
		o2 = bun.Ident("p2.executing_body_id")
	}

	uncompressed := nc.db.NewSelect().
		TableExpr("projects AS p1").
		ColumnExpr("? o1, ? o2, p1.joint_project_id jp, p1.funding_amount f1, p2.funding_amount f2", o1, o2).
		Join("JOIN projects AS p2").
		JoinOn("? < ?", o1, o2).
		JoinOn("p1.joint_project_id = p2.joint_project_id").
		Where("p1.joint_project_id IS NOT NULL")

	if len(params.LocationId) > 0 || len(params.ResearchAreaIds) > 0 {
		uncompressed = uncompressed.
			Join("JOIN organizations AS o1").Where("? = o1.id", o1).
			Join("JOIN organizations AS o2").Where("? = o2.id", o2)
	}

	if len(params.LocationId) > 0 {
		uncompressed = uncompressed.
			Where("o1.municipality_id LIKE ?", params.LocationId+"%").
			Where("o2.municipality_id LIKE ?", params.LocationId+"%")
	}

	if len(params.ResearchAreaIds) > 0 {
		uncompressed = uncompressed.
			Join("JOIN research_topics AS rt").JoinOn("rt.id = p1.research_topic_id").
			Where("rt.research_area_id IN (?)", bun.In(params.ResearchAreaIds))
	}

	if len(params.RecipientIds) > 0 {
		uncompressed = uncompressed.
			Where("p1.recipient_id IN (?)", bun.In(params.RecipientIds)).
			Where("p2.recipient_id IN (?)", bun.In(params.RecipientIds))
	}

	query := nc.db.NewSelect().With("uncompressed", uncompressed).
		Model(&multiEdges).
		ModelTableExpr("uncompressed").
		ColumnExpr("o1, o2, COUNT(DISTINCT jp) as multiplicity, SUM(f1) as f1, SUM(f2) as f2").
		Group("o1", "o2").
		Order("multiplicity DESC", "f1 + f2 DESC")

	if params.MaxEdges != 0 {
		query = query.Limit(params.MaxEdges)
	}

	err := query.Scan(ctx)

	nodes := make(map[int64]struct{})

	for _, me := range multiEdges {
		if params.MaxNodes > 0 {
			delta := 0
			if _, ok := nodes[me.O1]; !ok {
				delta += 1
			}
			if _, ok := nodes[me.O2]; !ok {
				delta += 1
			}
			if len(nodes)+delta > params.MaxNodes {
				continue
			}
			nodes[me.O1] = struct{}{}
			nodes[me.O2] = struct{}{}
		}

		nodeSet := NodeSet{
			[]int64{me.O1, me.O2},
			int(me.Multiplicity),
			[]decimal.Decimal{me.F1, me.F2},
		}
		nodeSets = append(nodeSets, nodeSet)
	}

	return nodeSets, err
}

type Controller struct {
	Org          *OrgController
	JointProject *JointProjectController
	Location     *LocationController
	Network      *NetworkController
}

func NewController(db *bun.DB) *Controller {
	return &Controller{
		Org:          &OrgController{db},
		JointProject: &JointProjectController{db},
		Location:     &LocationController{db},
		Network:      &NetworkController{db},
	}
}
