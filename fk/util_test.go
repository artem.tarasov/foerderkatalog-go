package fk

import "testing"

func TestNormalizeTopic(t *testing.T) {
	topic := "VP : - a für z Teilvorhaben:- A "
	expected := "a fur z teilprojekt a "
	tu := NewTrieUtil()
	if tu.NormalizeTopic(topic) != expected {
		t.Errorf("%s", tu.NormalizeTopic(topic))
	}

	topic = "Verbund: bbbbb; TP: B "
	expected = "bbbbb; teilprojekt b "
	if tu.NormalizeTopic(topic) != expected {
		t.Errorf("%v", tu.NormalizeTopic(topic))
	}
}

func TestCommonDescription(t *testing.T) {
	topics := []string{
		"VP epic - megaproject -, TP: A",
		"Verbundvorhaben: epic - megaproject , Teilvorhaben: B",
		"Verbundprojekt: epic - megaproject -, TP: C",
	}

	tu := NewTrieUtil()

	if tu.CommonDescription(topics) != "epic - megaproject" {
		t.Errorf("%s", tu.CommonDescription(topics))
	}

	topics = []string{
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: Adaptierter Radnabenantrieb für Dynamikmodul",
		"Verbundprojekt: Disruptive modulare Architektur für vielfältige, agile Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: IT Sicherheitsarchitektur für eine disruptive modulare Architektur für vielfältige, agile Fahrzeugkonzepte",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: Fliegende Umfeldsensorik: Info-Biene",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: AUTO-Vernetzung und AUTOtaxi",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: automatische 3D Kartierung mit Kameras für autonomes Fahren",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: Betrachtung und Methoden für Funktionale Sicherheit und Realisierung AUTOelfe",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: Konzeptionierung und Realisierung eines externen Umfeldmodells unter Einbringung der Simulationsdaten",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: modulare Absicherung, IT Sicherheitsarchitektur, Bewegungsregelung und sicheres Anhalten, AUTOelfe",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: Konzeptionierung und Umsetzung des automatisierten AUTOshuttles",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: UNICARagil Testumgebung",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: Entwurf und Umsetzung einer mechatronischen Architektur inkl. Thermomanagement für hochautomatisierte Fahrfunktionen",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: AUTOmatisierung, Open AUTOmated Driving Framework und AUTOliefer",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: Generische Sensormodule und automatisierte Umgebungswahrnehmung sowie Entwicklung eines autonomen Lieferfahrzeugs",
		"Verbundprojekt: Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil -, Teilvorhaben: Sichere Schätzung des Fahrdynamikzustands mit Selbstbeobachtung vermittels dedizierter Hardwareplattform",
	}

	expected := "Disruptive modulare Architektur für agile, autonome Fahrzeugkonzepte - UNICARagil"
	if tu.CommonDescription(topics) != expected {
		t.Errorf("%s", tu.CommonDescription(topics))
	}

}
