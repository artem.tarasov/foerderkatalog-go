package fk

import "testing"

func TestTrie(t *testing.T) {
	trie := NewTrie()

	// simple case
	trie.Add("storm")
	trie.Add("store")
	trie.Add("story")

	if trie.CommonPrefix() != "stor" {
		t.Fail()
	}

	// majority vote => ignores 'garbage'
	trie = NewTrie()
	trie.Add("storm")
	trie.Add("store")
	trie.Add("garbage")

	if trie.CommonPrefix() != "stor" {
		t.Fail()
	}

	// complete garbage
	trie = NewTrie()
	trie.Add("storm")
	trie.Add("trash")
	trie.Add("garbage")

	if trie.CommonPrefix() != "" {
		t.Fail()
	}

	// no strong majority => also empty
	trie = NewTrie()
	trie.Add("storm")
	trie.Add("store")
	trie.Add("trash")
	trie.Add("garbage")

	if trie.CommonPrefix() != "" {
		t.Fail()
	}
}
