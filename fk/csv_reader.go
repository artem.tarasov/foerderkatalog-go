package fk

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"
	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

// ="..." is what you often see in Excel
func removeEqualSign(s string) string {
	if strings.HasPrefix(s, "=\"") {
		return s[2 : len(s)-1]
	} else {
		return s
	}
}

type CsvReader struct {
	*csv.Reader
	organizations         map[string]*Organization
	researchTopics        map[string]*ResearchTopic
	jointProjects         map[string]*JointProject
	jointProjectAllTopics map[string][]string
}

func (csv CsvReader) GetOrganizations() []Organization {
	var orgs []Organization
	for _, v := range csv.organizations {
		orgs = append(orgs, *v)
	}
	return orgs
}

func (csv CsvReader) GetJointProjects(nThreads int) []JointProject {
	var descriptions sync.Map

	ch := make(chan string, nThreads)

	worker := func(input <-chan string) {
		tu := NewTrieUtil()
		for {
			if name, ok := <-input; !ok {
				return
			} else {
				description := tu.CommonDescription(csv.jointProjectAllTopics[name])
				descriptions.Store(name, description)
			}
		}
	}

	var wg sync.WaitGroup

	for i := 0; i < nThreads; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			worker(ch)
		}()
	}

	for name := range csv.jointProjects {
		ch <- name
	}
	close(ch)
	wg.Wait()

	var jps []JointProject
	for name, v := range csv.jointProjects {
		desc, _ := descriptions.Load(name)
		v.Description = desc.(string)
		jps = append(jps, *v)
	}
	return jps
}

func (csv CsvReader) GetResearchTopics() []ResearchTopic {
	var researchTopics []ResearchTopic
	for _, v := range csv.researchTopics {
		researchTopics = append(researchTopics, *v)
	}
	return researchTopics
}

func OpenCsv(filename string) (CsvReader, error) {
	f, err := os.Open(filename)
	if err != nil {
		return CsvReader{}, err
	}

	buf := bufio.NewReader(f)
	decoder := transform.NewReader(buf, charmap.Windows1252.NewDecoder())
	reader := csv.NewReader(decoder)
	reader.Comma = ';'
	reader.LazyQuotes = true

	return CsvReader{
		Reader:                reader,
		organizations:         make(map[string]*Organization),
		researchTopics:        make(map[string]*ResearchTopic),
		jointProjects:         make(map[string]*JointProject),
		jointProjectAllTopics: make(map[string][]string),
	}, nil
}

func (csv CsvReader) getOrganization(fields []string) *Organization {
	if strings.Contains(fields[0], "Keine Anzeige") {
		return nil // ignore weird entries
	}

	// Name is considered a unique identifier
	if org, ok := csv.organizations[fields[0]]; ok {
		return org
	}

	org := &Organization{
		ID:             int64(len(csv.organizations)),
		Name:           fields[0],
		MunicipalityId: fields[1], // Gemeindekennziffer
		Municipality:   fields[2], // Stadt/Gemeinde
		Location:       fields[3], // Ort
		State:          fields[4], // Bundesland
		Country:        fields[5], // Staat,
		Type:           string(GuessOrgType(fields[0])),
	}

	csv.organizations[fields[0]] = org
	return org
}

func (csv CsvReader) getJointProject(name string) *JointProject {
	if name == "" {
		return nil
	}

	if jp, ok := csv.jointProjects[name]; ok {
		return jp
	}

	jp := &JointProject{
		ID:   int64(len(csv.jointProjects)),
		Name: name,
	}

	csv.jointProjects[name] = jp
	return jp
}

func (csv CsvReader) getResearchTopic(code, description string) *ResearchTopic {
	if ra := getResearchAreaId(code); ra == nil {
		return nil
	} else if rt, ok := csv.researchTopics[code]; ok {
		return rt
	} else {
		// in the dataset, there are very few cases where there are differing
		// descriptions with the same code - let's just ignore them
		rt := &ResearchTopic{
			ID:             int64(len(csv.researchTopics)),
			Code:           code,
			Description:    description,
			ResearchAreaId: *ra,
		}

		csv.researchTopics[code] = rt
		return rt
	}
}

func (csv CsvReader) updateJointProject(jp *JointProject, p *Project) {
	if jp.ProjectCount == 0 {
		jp.StartDate = p.StartDate
		jp.EndDate = p.EndDate
		jp.FundingAmount = p.FundingAmount
		jp.ProjectCount = 1
	} else {
		if p.StartDate.Before(jp.StartDate) {
			jp.StartDate = p.StartDate
		}
		if p.EndDate.After(jp.EndDate) {
			jp.EndDate = p.EndDate
		}
		jp.FundingAmount = jp.FundingAmount.Add(p.FundingAmount)
		jp.ProjectCount += 1
	}

	csv.jointProjectAllTopics[jp.Name] = append(csv.jointProjectAllTopics[jp.Name], p.Topic)
}

func parseDate(s string) (time.Time, error) {
	return time.Parse("02.01.2006", s)
}

func (csv CsvReader) ReadProjects() ([]*Project, error) {
	var projects []*Project
	for {
		rawRecord, err := csv.Read()

		if err == io.EOF {
			break
		}

		if err != nil {
			return projects, err
		}

		record := make([]string, len(rawRecord))
		for i, field := range rawRecord {
			record[i] = removeEqualSign(field)
		}

		if record[0] == "FKZ" {
			continue // skip CSV header
		}

		recipient := csv.getOrganization(record[5:11])
		executingBody := csv.getOrganization(record[11:17])

		if recipient == nil || executingBody == nil {
			continue
		}

		fkz := record[0]
		projectTopic := record[17]

		if len(fkz) == 0 || strings.Contains(projectTopic, "Pseudovorhaben") {
			continue
		}

		researchTopic := csv.getResearchTopic(record[18], record[19])

		if researchTopic == nil {
			log.Warn().Str("FKZ", record[0]).Str("LPS", record[18]).Msg("invalid research topic, skipping")
			continue
		}

		// parse project duration
		var startDate, endDate time.Time
		if startDate, err = parseDate(record[20]); err != nil {
			log.Debug().Str("date", record[20]).Msg("failed to parse date")
			continue
		} else if endDate, err = parseDate(record[21]); err != nil {
			log.Debug().Str("date", record[21]).Msg("failed to parse date")
			continue
		}

		// parse funding amount (formatted as XXX,XXX.YY)
		fundingAmountStr := strings.ReplaceAll(record[22], ".", "")
		fundingAmountStr = strings.ReplaceAll(fundingAmountStr, ",", ".")
		var fundingAmount decimal.Decimal
		if fundingAmount, err = decimal.NewFromString(fundingAmountStr); err != nil {
			log.Debug().Str("value", record[22]).Msg("failed to parse amount")
			continue
		}

		jointProject := csv.getJointProject(record[24])

		var jointProjectId *int64
		if jointProject != nil {
			jointProjectId = &jointProject.ID
		}

		project := &Project{
			Fkz:             fkz,       // FKZ
			Ministry:        record[1], // Ressort
			Department:      record[2], // Referat
			ProjectSponsor:  record[3], // PT
			WorkUnit:        record[4], // Arb.-Einh
			RecipientId:     recipient.ID,
			ExecutingBodyId: executingBody.ID,
			Topic:           projectTopic,     // Thema
			ResearchTopicId: researchTopic.ID, // Leistungsplansystematik
			StartDate:       startDate,        // Laufzeit von
			EndDate:         endDate,          // Laufzeit bis
			FundingAmount:   fundingAmount,    // Foerdersumme in EUR
			FundingProfile:  record[23],       // Foerderprofil
			JointProjectId:  jointProjectId,   // Verbundprojekt
			FundingType:     record[25],       // Foerderart
		}

		if jointProject != nil {
			csv.updateJointProject(jointProject, project)
		}

		projects = append(projects, project)
	}

	return projects, nil
}
