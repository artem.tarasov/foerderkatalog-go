package main

import (
	"devopps.de/fk/cmd"
)

func main() {
	cmd.Execute()
}
