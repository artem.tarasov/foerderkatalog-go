# Foerderkatalog

German government provides information about publicly funded projects at
https://foerderportal.bund.de/foekat/jsp/StartAction.do

This information can be downloaded as a giant CSV file from the "Vorhabensuche" tab,
if one clicks on "Ausgabe als Textdatei" in the search results.

The table is not at all normalized, which makes it hard to work with the data. 
This is where this project aims to help.

## Usage

### Data import

Download the CSV file from the website (it's named "Suchliste.csv" by default).
Install the executable: `go install fk.go` will put it to `~/go/bin/fk`.
Run `~/go/bin/fk import` - in a few seconds it will create `fk.sqlite` file.

You can use the database directly, e.g.:

```
$ sqlite3 fk.sqlite
SQLite version 3.38.3 2022-04-27 12:03:15
Enter ".help" for usage hints.
sqlite> SELECT r.name, eb.name, COUNT(*) n FROM projects 
   ...> JOIN organizations r ON r.id = recipient_id 
   ...> JOIN organizations eb ON eb.id = executing_body_id
   ...> WHERE eb.municipality_id LIKE '11%'
   ...> GROUP BY recipient_id, executing_body_id
   ...> ORDER BY n DESC LIMIT 10;
Bundesanstalt für Materialforschung und -prüfung (BAM)|Bundesanstalt für Materialforschung und -prüfung (BAM)|42
Ferdinand-Braun-Institut gGmbH, Leibniz- Institut für Höchstfrequenztechnik|Ferdinand-Braun-Institut gGmbH, Leibniz- Institut für Höchstfrequenztechnik|42
Institut für ökologische Wirtschaftsforschung (IÖW) GmbH|Institut für ökologische Wirtschaftsforschung (IÖW) GmbH|37
Fraunhofer-Gesellschaft zur Förderung der angewandten Forschung eingetragener Verein|Fraunhofer-Institut für Nachrichtentechnik - Heinrich-Hertz-Institut (HHI)|32
Fraunhofer-Gesellschaft zur Förderung der angewandten Forschung eingetragener Verein|Fraunhofer-Institut für Zuverlässigkeit und Mikrointegration (IZM)|32
Helmholtz-Zentrum Berlin für Materialien und Energie Gesellschaft mit beschränkter Haftung|Helmholtz-Zentrum Berlin für Materialien und Energie Gesellschaft mit beschränkter Haftung|31
Hochschule für Technik und Wirtschaft Berlin|Hochschule für Technik und Wirtschaft Berlin|29
Max-Delbrück-Centrum für Molekulare Medizin in der Helmholtz-Gemeinschaft (MDC)|Max-Delbrück-Centrum für Molekulare Medizin in der Helmholtz-Gemeinschaft (MDC)|25
Adelphi Research gemeinnützige GmbH|Adelphi Research gemeinnützige GmbH|21
Humboldt-Universität zu Berlin|Humboldt-Universität zu Berlin - Mathematisch-Naturwissenschaftliche Fakultät - Institut für Physik|20
```

### Webserver

Another subcommand, `fk serve`, starts a webserver on `localhost:3000`.
It currently provides the following endpoints:

- Lookup an organization by name: `/organizations/find?name=<QUERY>`
- Lookup an organization by id: `/organizations/:id`
- List projects that an organization participates in: `/organizations/:id/projects`
- Lookup a joint project by id: `/joint-projects/:id`
- List parts of a joint project: `/joint-projects/:id/projects`
- List others' parts of the joint projects an organization is executing: `/organizations/:id/collaborations`
- List organizations in a particular location: `/location/:id/organizations`
- List projects executed in a particular location: `/location/:id/projects` 

## License

This work is licensed under Apache 2.0.
